#!/usr/bin/python3

import os
from distutils.dir_util import copy_tree


paths=os.environ['CONFIG_TEMPLATES'].split("#")

for path in paths:
	configPath=path.split(":")
	print(configPath)

	if os.listdir(configPath[1]) == []:
		copy_tree(configPath[0], configPath[1])
